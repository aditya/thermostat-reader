# README

### Local Development Setup

Prerequisites:
- Install [Ruby version 2.6.1](https://www.ruby-lang.org/en/news/2019/01/30/ruby-2-6-1-released/)
- Install Bundler, `gem install bundler`
- Install [PostgreSQL](https://wiki.postgresql.org/wiki/Homebrew)
- Install [Redis](https://redis.io/)

Installation:
```
cd thermostat-reader
bundle install
bundle exec rake db:setup
gem install foreman
foreman start -f Procfile.dev
```
Note: This will also seed Thermostat records in the database

Running Test Cases:

Start Redis For Test Environment:

```
cd thermostat-reader
foreman start -f Procfile.test
```

Run Test Cases with Rspec:
```
rspec spec
```


### Application Details

Application has two main entities:
```
1. Thermostat(id: integer, household_token: string, location: text, created_at: datetime, updated_at: datetime)

2. Reading(id: integer, thermostat_id: integer, tracking_number: integer, temperature: float, humidity: float, battery_charge: float, created_at: datetime, updated_at: datetime)
```

APIs:

Note: Each API expects Thermostat household token as Header to authenticate Thermostat



1. POST Reading: collects temperature, humidity and battery charge from a particular thermostat.
This method is going to have a very high request rate because many IoT thermostats are going to call
it very frequently and simultaneously. Make it as fast as possible and schedule a background job
for writing to the DB (you can use Sidekiq for example). The method also returns a generated tracking
number starting from 1 and every household has its own tracking sequence.

    Example Request:
    ```
    post '/v1/readings',
      params: {
        "temperature" => 21.45,
        "humidity" => 34.68,
        "battery_charge" => 92.70
      },
      headers: {
        "Household-Token" => "5a9ac82f-5f41-47f9-8c20-a3e61c5953bc"
      }
    ```
    
    Example Sucessful Response Body:
    ```
    { "tracking_number" => "21" }
    ```

2. GET Reading: returns the thermostat reading data by the tracking number obtained from POST
Reading. The API must be consistent, that is if the method 1 returns, the thermostat data must be
immediately available from this method even if the background job is not yet finished.

    Example Request:
    ```
    get '/v1/readings/{tracking_number}',
      headers: {
        "Household-Token" => "5a9ac82f-5f41-47f9-8c20-a3e61c5953bc"
      }
    ```
    
    Example Sucessful Response Body:
    ```
    {"reading"=>{"tracking_number"=>1, "temperature"=>-9.360890991032754, "humidity"=>71.33563769542735, "battery_charge"=>68.36175509425846}}
    ```

3. GET Stats: gives the average, minimum and maximum by temperature, humidity and
battery_charge in a particular thermostat across all the period of time. Make sure it executes in
O(1) time and it is consistent in the same way as method 2.

    Example Request:
    ```
    get '/v1/readings/{tracking_number}',
      headers: {
        "Household-Token" => "5a9ac82f-5f41-47f9-8c20-a3e61c5953bc"
      }
    ```
    
    Example Sucessful Response Body:
    ```
    "stats"=>{
      "temperature"=>{"average"=>"10.55384937606022", "minimum"=>"10.55384937606022", "maximum"=>"10.55384937606022"},
      "humidity"=>{"average"=>"48.843145736997414", "minimum"=>"48.843145736997414", "maximum"=>"48.843145736997414"},
      "battery_charge"=>{"average"=>"55.91127842281065", "minimum"=>"55.91127842281065", "maximum"=>"55.91127842281065"}
    }
    ```