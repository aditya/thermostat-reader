# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_21_065334) do

  create_table "readings", force: :cascade do |t|
    t.integer "thermostat_id", null: false
    t.integer "tracking_number", null: false
    t.float "temperature", null: false
    t.float "humidity", null: false
    t.float "battery_charge", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["thermostat_id"], name: "index_readings_on_thermostat_id"
    t.index ["tracking_number", "thermostat_id"], name: "index_readings_on_tracking_number_and_thermostat_id", unique: true
  end

  create_table "thermostats", force: :cascade do |t|
    t.string "household_token", null: false
    t.text "location"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "readings", "thermostats"
end
