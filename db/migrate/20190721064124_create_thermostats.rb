class CreateThermostats < ActiveRecord::Migration[6.0]
  def change
    create_table :thermostats do |t|
      t.string :household_token, null: false
      t.text :location

      t.timestamps
    end
  end
end
