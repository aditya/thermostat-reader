class CreateReadings < ActiveRecord::Migration[6.0]
  def change
    create_table :readings do |t|
      t.references :thermostat, null: false, foreign_key: true
      t.integer :tracking_number, null: false
      t.float :temperature, null: false
      t.float :humidity, null: false
      t.float :battery_charge, null: false

      t.timestamps
    end
    add_index :readings, [:tracking_number, :thermostat_id], unique: true
  end
end
