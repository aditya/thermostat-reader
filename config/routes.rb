Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :v1 do
    resources :readings, only: [:create, :show], param: :tracking_number
    get 'thermostats/stats'
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end
