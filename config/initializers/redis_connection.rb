$redis = Redis.new(
  host: ENV["redis_host"] || "127.0.0.1", 
  port: Rails.env.test? ? "6380" : (ENV["redis_port"] || "6379")
)