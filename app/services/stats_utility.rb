module StatsUtility
  def stat_average(val1: 0, val2: 0)
    return val2 unless val1.present?
    (val1.to_f + val2.to_f) / 2
  end

  def stat_minimum(val1: 0, val2: 0)
    return val2 unless val1.present?
    (val1.to_f > val2.to_f ? val2 : val1).to_f
  end

  def stat_maximum(val1: 0, val2: 0)
    return val2 unless val1.present?
    (val1.to_f > val2.to_f ? val1 : val2).to_f
  end
end