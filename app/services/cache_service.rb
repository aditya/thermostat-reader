class CacheService
  class << self
    def set_hash_field(key:, field:, value:)
      redis.hset(key, field, value)
    end

    def get_hash_field(key:, field:)
      redis.hget(key, field)
    end

    def get_hash(key)
      redis.hgetall(key)
    end

    def set_hash(key:, fields: {})
      return false if fields.blank?
      redis.hmset(key, fields.flatten)
    end

    def get_key(key)
      redis.get(key)
    end

    def increment_key(key)
      # Increments the key counter
      # Sets the key and defaults the value to 1 if given key is not present
      redis.incr(key)
    end

    def delete_hash_field(key:, field:)
      if redis.hexists(key, field)
        redis.hdel(key, field)
      end
    end


    private

    def redis
      $redis
    end
  end
end