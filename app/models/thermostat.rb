class Thermostat < ApplicationRecord
  include StatsUtility

  validates :household_token, presence: true

  has_many :readings

  STATS_KEYS = [:temperature, :humidity, :battery_charge]

  STATS_PARAMS = [:average, :minimum, :maximum]

  def reading_tracker_key
    "#{self.household_token}/tracker"
  end

  def reading_data_key
    "#{self.household_token}/data"
  end

  def stats_key(parameter)
    "#{self.household_token}/stats/#{parameter}"
  end

  def stats
    STATS_KEYS.inject({}) do |h, parameter|
      key = stats_key(parameter)
      h[parameter] = CacheService.get_hash(key).presence
      h
    end
  end

  # Updates Thermostat stats (average, min, max) for parameters (temperature, humidity, battery_charge)
  # Example:
  # Input: tracker_count: 5, params: { temperature: 20.0, humidity: 14.23, battery_charge: 84.0 }
  # Data is saved in following format:
  #
  # Temperature
  # { "11717129-f96f-4523-8280-a26875764d03/stats/temperature" =>
  #  {
  #   average: "22.2", min: "19.23", max: "24.4"
  #  }
  # }
  # Humidity
  # { "11717129-f96f-4523-8280-a26875764d03/stats/temperature" =>
  #  {
  #   average: "12.2", min: "11.0", max: "18.2"
  #  }
  # }
  # Battery Charge
  # { "11717129-f96f-4523-8280-a26875764d03/stats/temperature" =>
  #  {
  #   average: "74.23", min: "45.8", max: "92.8"
  #  }
  # }
  #

  def update_stats_from_params(params:)
    STATS_KEYS.each do |parameter|
      key = stats_key(parameter)
      previous_data = CacheService.get_hash(key).symbolize_keys
      stats_hash = calculate_stats(previous_data: previous_data, value: params[parameter])
      CacheService.set_hash(key: key, fields: stats_hash)
    end
  end

  private

  def calculate_stats(previous_data:, value:)
    # stat_average(val1: previous_data[:average], val2: value)
    # stat_minimum(val1: previous_data[:minimum], val2: value)
    # stat_maximum(val1: previous_data[:maximum], val2: value)
    STATS_PARAMS.inject({}) do |h, param|
      h[param] = self.send("stat_#{param}", { val1: previous_data[param], val2: value })
      h
    end
  end
end
