class Reading < ApplicationRecord
  belongs_to :thermostat, required: true
  validates :temperature, :humidity, :battery_charge, presence: true
  validates :tracking_number, presence: true, uniqueness: { scope: :thermostat_id }


  class << self
    # Caches the reading data and enqueues a job to create Reading from given data
    # The cached data is stored in JSON format against a thermostat specific key and tracking number as key
    # Eg: When tracking number is 20, and thermostat key is "6b838a03-5cbd-4f1d-ad58-4d92b899939b/data" data is stored in following format
    # {
    #   "6b838a03-5cbd-4f1d-ad58-4d92b899939b/data" => {
    #     "20" => "{\"temperature\":-4.056420626633916,\"humidity\":93.99042646114427,\"battery_charge\":18.764593162339438}"
    #   }
    # }
    #
    # Also increments the reading tracker against a thermostat
    # Eg: { "6b838a03-5cbd-4f1d-ad58-4d92b899939b/tracker" => 20 }
    #
    # Removes the cached reading data after the background job is executed
    # Does not remove the reading tracker from cache, the tracker is kept persistent
    # and increases with every Reading addition for a thermostat
    def cache_and_create(params:, thermostat: thermostat)
      tracker = cache_from_params(params, thermostat)
      CreateReadingJob.perform_later(
                        thermostat: thermostat, params: params.merge(tracking_number: tracker))
      tracker
    end

    # Fetches data from tracking number
    # Finds the data first in cache, if data is found, rerturns a new record with cache data
    # If data is not found in the cache, finds the data from database
    # Raises ActiveRecord::RecordNotFound if not daa is found
    def from_tracking_number!(tracking_number:, thermostat: thermostat)
      reading = record_from_cached_data(tracking_number, thermostat)
      return reading if reading.present?
      self.find_by!(tracking_number: tracking_number)
    end

    private

    def cache_from_params(params, thermostat)
      tracker = increment_tracker(thermostat.reading_tracker_key)
      CacheService.set_hash_field(
                    key: thermostat.reading_data_key, field: tracker, value: params.to_json)
      tracker
    end

    def increment_tracker(key)
      # Sets the key and defaults the value to 1 if given key is not present
      # Returns the incremented tracker value
      CacheService.increment_key(key)
    end

    def record_from_cached_data(tracking_number, thermostat)
      reading_data = CacheService.get_hash_field(key: thermostat.reading_data_key, field: tracking_number)
      if reading_data.present?
        reading_data = JSON.parse(reading_data)
        Reading.new(reading_data.merge(tracking_number: tracking_number))
      end
    end
  end

  def clear_cache
    CacheService.delete_hash_field(key: thermostat.reading_data_key, field: self.tracking_number)
  end
end
