# frozen_string_literal: true

class CreateReadingJob < ApplicationJob
  queue_as :default

  def perform(thermostat:, params:)
    reading = thermostat.readings.create!(params)
    reading.clear_cache
  end
end
