class V1::ThermostatsController < ApplicationController
  # Example Response Format:
  # {
  #   "stats"=>{
  #     "temperature"=>{"average"=>"10.55384937606022", "minimum"=>"10.55384937606022", "maximum"=>"10.55384937606022"},
  #     "humidity"=>{"average"=>"48.843145736997414", "minimum"=>"48.843145736997414", "maximum"=>"48.843145736997414"},
  #     "battery_charge"=>{"average"=>"55.91127842281065", "minimum"=>"55.91127842281065", "maximum"=>"55.91127842281065"}
  #   }
  # }
  def stats
    stats = thermostat.stats
    render json: { stats: stats }, status: :ok
  end
end
