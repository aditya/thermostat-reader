class V1::ReadingsController < ApplicationController

  after_action :update_thermostat_stats

  def create
    tracking_number = Reading.cache_and_create(params: reading_params, thermostat: thermostat)
    render json: { tracking_number: tracking_number }, status: :ok
  end

  def show
    reading = Reading.from_tracking_number!(
                        tracking_number: params[:tracking_number], thermostat: thermostat)
    render json: reading, status: :ok
  end

  private

  def reading_params
    params.require(:reading).permit(:temperature, :humidity, :battery_charge).tap do |reading_info|
      reading_info.require([:temperature, :humidity, :battery_charge])
    end
  end

  def update_thermostat_stats
    thermostat.update_stats_from_params(params: params)
  end
end
