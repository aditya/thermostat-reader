class ApplicationController < ActionController::API

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  rescue_from ActionController::ParameterMissing, with: :missing_parameters

  before_action :authenticate_thermostat

  attr_reader :thermostat

  private

  def authenticate_thermostat
    @thermostat = Thermostat.find_by_household_token(request.headers["Household-Token"])
    render json: { error: I18n.t("auth.household_token.invalid") }, status: 401 unless @thermostat
  end

  def record_not_found
    render json: { error: I18n.t("errors.record_not_found") }, status: :not_found
  end

  def missing_parameters
    render json: { error: I18n.t("errors.missing_parameters") }, status: :unprocessable_entity
  end
end
