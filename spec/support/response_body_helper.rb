module ResponseBodyHelper
  def parse_json_body(response)
    JSON.parse(response.body)
  end
end

RSpec.configure do |config|
  config.include ResponseBodyHelper
end