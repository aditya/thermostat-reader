FactoryBot.define do
  factory :reading do
    thermostat
    sequence :tracking_number do |n|
      n
    end
    temperature { Faker::Number.between(-30.0, 30.0) }
    humidity { Faker::Number.between(0, 100.0) }
    battery_charge { Faker::Number.between(0, 100.0) }
  end
end
