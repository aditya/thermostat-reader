FactoryBot.define do
  factory :thermostat do
    household_token { SecureRandom.uuid }
    location { Faker::Address.full_address }
  end
end
