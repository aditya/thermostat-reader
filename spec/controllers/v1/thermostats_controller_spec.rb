require 'rails_helper'

RSpec.describe V1::ThermostatsController, type: :controller do

  describe "GET #stats" do

    let(:thermostat) { create(:thermostat) }

    before :each do
      household_token = thermostat.household_token
      request.headers["Household-Token"] = household_token
    end

    context "invalid household token" do
      it "returns forbidden error" do
        request.headers["Household-Token"] = nil
        get :stats
        expect(response).to have_http_status(401)
      end
    end

    context "when no existing stats data is present" do
      it "returns stats with nil data" do
        get :stats
        expect(response).to have_http_status(:success)
        parsed_body = parse_json_body(response).deep_symbolize_keys
        expect(parsed_body[:stats].keys).to eq(Thermostat::STATS_KEYS)
        Thermostat::STATS_KEYS.each do |key|
          expect(parsed_body[:stats][key]).to eq(nil)
        end
      end
    end

    context "when existing reading data is present" do
      let(:params) do
        {
          temperature: Faker::Number.between(-30.0, 30.0),
          humidity: Faker::Number.between(0, 100.0),
          battery_charge: Faker::Number.between(0, 100.0)
        }
      end
      before do
        thermostat.update_stats_from_params(params: params)
        get :stats
      end
      it "returns stats data with correct keys" do
        expect(response).to have_http_status(:success)
        parsed_body = parse_json_body(response).deep_symbolize_keys
        expect(parsed_body[:stats].keys).to eq(Thermostat::STATS_KEYS)
        Thermostat::STATS_KEYS.each do |key|
          expect(parsed_body[:stats][key].keys).to eq(Thermostat::STATS_PARAMS)
        end
      end
    end
  end
end
