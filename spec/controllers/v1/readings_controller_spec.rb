require 'rails_helper'

RSpec.describe V1::ReadingsController, type: :controller do
  let(:params) do
    {
      temperature: Faker::Number.between(-30.0, 30.0),
      humidity: Faker::Number.between(0, 100.0),
      battery_charge: Faker::Number.between(0, 100.0)
    }
  end

  let(:thermostat) { create(:thermostat) }

  before :each do
    household_token = thermostat.household_token
    request.headers["Household-Token"] = household_token
  end

  describe "POST #create" do

    context "invalid household token" do
      it "returns forbidden error" do
        request.headers["Household-Token"] = nil
        post :create, params: { reading: params }
        expect(response).to have_http_status(401)
      end
    end

    context "valid household token" do
      context "invalid params" do
        it "fails without required parameters" do
          post :create, params: { reading: {} }
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
      context "valid params" do
        it "returns http success" do
          post :create, params: { reading: params }
          expect(response).to have_http_status(:success)
        end

        it "returns a tracking number and updates it on second request" do
          post :create, params: { reading: params }
          expect(response).to have_http_status(:success)
          tracking_number = parse_json_body(response)["tracking_number"].to_i
          post :create, params: { reading: params }
          next_tracking_number = parse_json_body(response)["tracking_number"].to_i
          expect(next_tracking_number).to eq(tracking_number + 1)
        end

        it "updates thermostat stats" do
          post :create, params: { reading: params }
          Thermostat::STATS_KEYS.each do |stat_key|
            data = CacheService.get_hash(thermostat.stats_key(stat_key))
            expect(data).not_to be_empty
            expect(data.symbolize_keys.keys).to eq(Thermostat::STATS_PARAMS)
          end
        end
      end
    end
  end

  describe "GET #reading" do
    context "invalid tracking number" do
      it "returns not found error" do
        get :show, params: { tracking_number: Faker::Number.number(3) }
        expect(response).to have_http_status(:not_found)
      end
    end

    context "valid tracking number" do
      it "returns the reading record from tracking number" do
        post :create, params: { reading: params }
        tracking_number = parse_json_body(response)["tracking_number"].to_i
        get :show, params: { tracking_number: tracking_number }
        expect(response).to have_http_status(:success)
        json_response = parse_json_body(response)
        expect(json_response["reading"]).not_to be_empty
        expect(json_response.dig("reading", "tracking_number")).to eq(tracking_number)
      end
    end
  end
end
