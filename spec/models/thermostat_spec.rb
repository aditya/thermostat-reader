require 'rails_helper'

RSpec.describe Thermostat, type: :model do
  describe '#validations' do
    it { is_expected.to validate_presence_of(:household_token) }
  end

  describe "instance methods" do
    let(:thermostat) { create(:thermostat) }
    let(:params) do
      {
        temperature: Faker::Number.between(-30.0, 30.0),
        humidity: Faker::Number.between(0, 100.0),
        battery_charge: Faker::Number.between(0, 100.0)
      }
    end

    describe ".update_stats_from_params" do
      subject(:update_stats_from_params) { thermostat.update_stats_from_params(params: params) }

      context "no existing cache data" do
        it "updates stats in cache and has same values for all stats" do
          update_stats_from_params
          Thermostat::STATS_KEYS.each do |stat_key|
            data = CacheService.get_hash(thermostat.stats_key(stat_key))
            expect(data).not_to be_empty
            expect(data.symbolize_keys.keys).to eq(Thermostat::STATS_PARAMS)
            expect(data.values.uniq.first).to eq(params[stat_key].to_s)
          end
        end
      end

      context "existing cache data" do
        let(:old_params) do
          {
            temperature: 24.8,
            humidity: 30.4,
            battery_charge: 88.0
          }
        end
        let(:new_params) do
          {
            temperature: 22.2,
            humidity: 40.7,
            battery_charge: 43.4
          }
        end
        let(:expected_result) do
          {
            temperature: {"average"=>"23.5", "minimum"=>"22.2", "maximum"=>"24.8"},
            humidity: {"average"=>"35.55", "minimum"=>"30.4", "maximum"=>"40.7"},
            battery_charge: {"average"=>"65.7", "minimum"=>"43.4", "maximum"=>"88.0"}
          }
        end
        it "calculates different stats and updates in the cache" do
          thermostat.update_stats_from_params(params: old_params)
          thermostat.update_stats_from_params(params: new_params)
          Thermostat::STATS_KEYS.each do |stat_key|
            data = CacheService.get_hash(thermostat.stats_key(stat_key))
            expect(data).to eq(expected_result[stat_key])
          end
        end
      end
    end
  end
end
