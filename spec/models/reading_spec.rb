require 'rails_helper'

RSpec.describe Reading, type: :model do
  include ActiveJob::TestHelper

  describe "validations" do
    it { is_expected.to validate_presence_of(:temperature) }
    it { is_expected.to validate_presence_of(:humidity) }
    it { is_expected.to validate_presence_of(:battery_charge) }
    it { is_expected.to validate_presence_of(:tracking_number) }
    it { should belong_to(:thermostat).required(true) }
    subject { create(:reading) }
    it { is_expected.to validate_uniqueness_of(:tracking_number).scoped_to(:thermostat_id) }
  end

  describe "class methods" do
    let(:params) do
      {
        temperature: Faker::Number.between(-30.0, 30.0),
        humidity: Faker::Number.between(0, 100.0),
        battery_charge: Faker::Number.between(0, 100.0)
      }
    end
    let(:thermostat) { create(:thermostat) }
    let(:reading) { create(:reading) }
    subject(:cache_and_create) { Reading.cache_and_create(params: params, thermostat: thermostat) }

    describe ".cache_and_create" do
      context "before reading db create job execution" do
        it "caches the reading data" do
          expect{ cache_and_create }.to change{ CacheService.get_key(thermostat.reading_tracker_key).to_i }.by(1)
          expect(CacheService.get_hash_field(key: thermostat.reading_data_key, field: subject)).to eq(params.to_json)
        end

        it "does not save the Reading data to database right away" do
          expect { cache_and_create }.to_not change { Reading.count }
        end

        it "enqueues the create reading data job" do
          expect { cache_and_create }.to enqueue_job(CreateReadingJob)
          expect(CreateReadingJob).to have_been_enqueued
            .with({thermostat: thermostat, params: params.merge(tracking_number: subject)})
            .on_queue("default")
        end
      end

      context "after reading db create job execution" do

        it "creates reading in db" do
          cache_and_create
          expect { perform_enqueued_jobs }.to change{ Reading.count }.by(1)
          reading = Reading.last
          expect(reading.temperature).to eq(params[:temperature])
          expect(reading.humidity).to eq(params[:humidity])
          expect(reading.battery_charge).to eq(params[:battery_charge])
          expect(reading.thermostat).to eq(thermostat)
        end

        it "removes cached reading data" do
          cache_and_create
          perform_enqueued_jobs
          expect(CacheService.get_hash_field(key: thermostat.reading_data_key, field: subject)).to eq(nil)
        end

        it "does not remove reading tracker from cached data" do
          cache_and_create
          expect(CacheService.get_key(thermostat.reading_tracker_key)).not_to be_empty
          expect { perform_enqueued_jobs }.to_not change { CacheService.get_key(thermostat.reading_tracker_key) }
        end
      end
    end

    describe ".from_tracking_number!" do
      subject(:from_tracking_number!) do
        Reading.from_tracking_number!(tracking_number: tracking_number, thermostat: thermostat)
      end
      context "invalid tracking number" do
        let(:tracking_number) { nil }
        it "raises record not found error" do
          expect { from_tracking_number! }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end

      context "valid tracking number" do
        context "with cached data" do
          let(:tracking_number) { cache_and_create }
          it "returns record from cached data" do
            expect(from_tracking_number!).to be_an_instance_of(Reading)
          end
          it "is not a database record" do
            expect(subject.persisted?).to eq(false)
          end
          it "has correct data" do
            expect(subject.temperature).to eq(params[:temperature])
            expect(subject.humidity).to eq(params[:humidity])
            expect(subject.battery_charge).to eq(params[:battery_charge])
          end
        end
        context "without cached data" do
          let(:tracking_number) { reading.tracking_number }
          it "returns record from database" do
            expect(from_tracking_number!).to be_an_instance_of(Reading)
            expect(subject).to eq(reading)
          end
        end
      end
    end
  end
end
